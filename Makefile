CC=cc
CFLAGS=-g3
#LDFLAGS=-fuse-ld=gold -Wl,--no-ctors-in-init-array
LDFLAGS=-fuse-ld=lld

%.o : %.c
	${CC} -c ${CFLAGS} $<

%.o : %.S
	${CC} -c ${CFLAGS} $<

test : test.o main.o
	${CC} ${LDFLAGS} -o $@ $+

clean :
	rm -f *.o
