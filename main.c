#include <stdio.h>
#include "test.h"

static const char *get_kind_str(int kind) {
    switch (kind) {
    case KIND_INIT: return ".init";
    case KIND_CTORS: return ".ctors";
    case KIND_DTORS: return ".dtors";
    case KIND_INIT_ARRAY: return ".init_array";
    case KIND_FINI_ARRAY: return ".fini_array";
    default: return "unknown";
    }
}

void test(int kind, int n, int m) {
    printf("%s %d %d\n", get_kind_str(kind), n, m);
}

int main() {
    printf("main\n");
    return 0;
}
