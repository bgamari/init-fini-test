#include "test.h"

#if defined(WINDOWS)
#define SECTION_TAG(tag)
#define ARG1 %rcx
#define ARG2 %rdx
#define ARG3 %r8
#else
#define SECTION_TAG(tag) , tag
#define ARG1 %rdi
#define ARG2 %rsi
#define ARG3 %rdx
#endif

#define TEST(name, kind, n, m) \
    .section .text;            \
    name:                      \
        movq $kind, ARG1;      \
        mov $n, ARG2;          \
        mov $m, ARG3;          \
        jmp test

#if 0
.section .init
TEST(init1, KIND_INIT, 0, 0)
#endif

#if 1
TEST(init_array_0,     KIND_INIT_ARRAY, 0,    0)
TEST(init_array_1,     KIND_INIT_ARRAY, 0,    1)
TEST(init_array1000_0, KIND_INIT_ARRAY, 1000, 0)
TEST(init_array1000_1, KIND_INIT_ARRAY, 1000, 1)
TEST(init_array2000_0, KIND_INIT_ARRAY, 2000, 0)
TEST(init_array2000_1, KIND_INIT_ARRAY, 2000, 1)

.section .init_array, "aw" SECTION_TAG(@init_array)
.align 8
.global init_array
init_array:
    .quad init_array_0
    .quad init_array_1

.section .init_array.1000, "aw" SECTION_TAG(@init_array)
.align 8
    .quad init_array1000_0
    .quad init_array1000_1

.section .init_array.2000, "aw" SECTION_TAG(@init_array)
.align 8
    .quad init_array2000_0
    .quad init_array2000_1
#endif

#if 1
TEST(fini_array_0,     KIND_FINI_ARRAY, 0,    0)
TEST(fini_array_1,     KIND_FINI_ARRAY, 0,    1)
TEST(fini_array1000_0, KIND_FINI_ARRAY, 1000, 0)
TEST(fini_array1000_1, KIND_FINI_ARRAY, 1000, 1)
TEST(fini_array2000_0, KIND_FINI_ARRAY, 2000, 0)
TEST(fini_array2000_1, KIND_FINI_ARRAY, 2000, 1)

.section .fini_array, "aw" SECTION_TAG(@fini_array)
.align 8
.global fini_array
fini_array:
    .quad fini_array_0
    .quad fini_array_1

.section .fini_array.1000, "aw" SECTION_TAG(@fini_array)
.align 8
    .quad fini_array1000_0
    .quad fini_array1000_1

.section .fini_array.2000, "aw" SECTION_TAG(@fini_array)
.align 8
    .quad fini_array2000_0
    .quad fini_array2000_1
#endif

#if 1
TEST(ctors_0,     KIND_CTORS, 0,    0)
TEST(ctors_1,     KIND_CTORS, 0,    1)
TEST(ctors1000_0, KIND_CTORS, 1000, 0)
TEST(ctors1000_1, KIND_CTORS, 1000, 1)
TEST(ctors2000_0, KIND_CTORS, 2000, 0)
TEST(ctors2000_1, KIND_CTORS, 2000, 1)

.section .ctors, "w"
.align 8
.global ctors
ctors:
    .quad ctors_0
    .quad ctors_1

.section .ctors.1000
.align 8
    .quad ctors1000_0
    .quad ctors1000_1

.section .ctors.2000
.align 8
    .quad ctors2000_0
    .quad ctors2000_1
#endif

#if 1
TEST(dtors_0,     KIND_DTORS, 0,    0)
TEST(dtors_1,     KIND_DTORS, 0,    1)
TEST(dtors1000_0, KIND_DTORS, 1000, 0)
TEST(dtors1000_1, KIND_DTORS, 1000, 1)
TEST(dtors2000_0, KIND_DTORS, 2000, 0)
TEST(dtors2000_1, KIND_DTORS, 2000, 1)

.section .dtors, "w"
.align 8
.global dtors
dtors:
    .quad dtors_0
    .quad dtors_1

.section .dtors.1000
.align 8
    .quad dtors1000_0
    .quad dtors1000_1

.section .dtors.2000
.align 8
    .quad dtors2000_0
    .quad dtors2000_1
#endif

